import java.io.*;
import java.net.*;

//client which can use 'list', 'get' and 'put' commands to read and transfer files via sockets
public class Client {
	private Socket clientSocket = null;
	private BufferedWriter socketOutput = null;		//to write to server
	private BufferedReader socketInput = null;		//to read from server
	private String fileName = null;				//read filenames for list command
	private String fileExists = null;			//check from server if file exists
	private File path = null;							//file to write contents into from server
	private BufferedOutputStream getFile = null;	//get file from server
	private BufferedInputStream putFile = null;		//send file to server
	private String filePath = null;				//path of the file in clientFiles

	public Client() {
		try {
			clientSocket = new Socket("localhost", 8888);
			socketInput = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			socketOutput = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
		}
		catch (UnknownHostException ex) {
			System.err.println("Don't know about host\n");
			System.exit(1);
		}
		catch (IOException ex) {
				System.err.println("Couldn't get I/O for the connection to host\n");
				System.exit(1);
		}
	}

	//lists all of the files in the serverFiles folder
	public void list(String command) {
		try {
			//send command to server
			socketOutput.write(command);
			socketOutput.newLine();
			socketOutput.flush();

			//read until no more files
			while ((fileName = socketInput.readLine()) != null) {
				System.out.println(fileName);
			}
			socketOutput.close();
			socketInput.close();
			clientSocket.close();
		}
		catch (IOException ex) {
			System.err.println("I/O exception during execution");
			System.exit(1);
		}
	}

	//requests the server to send a file and saves it in the clientFiles folder
	public void get(String[] command) {
		try {
			socketOutput.write(command[0]);
			socketOutput.newLine();
			socketOutput.flush();

			//send filename to server
			socketOutput.write(command[1]);
			socketOutput.newLine();
			socketOutput.flush();

			//if file does not exist in server, then quit
			fileExists = socketInput.readLine();
			if (fileExists.equals("File does not exist")) {
				System.err.println(fileExists);
				System.exit(1);
			}

			path = new File("clientFiles/" + command[1]);
			getFile = new BufferedOutputStream(new FileOutputStream(path));

			//temporary variable to store what is read from the server
			int temp = 0;
			//read from server and write to file in clientFiles
			while ((temp = socketInput.read()) != -1) {
				getFile.write(temp);
			}
			getFile.flush();
			getFile.close();
			System.out.println("File received");

			socketOutput.close();
			socketInput.close();
			clientSocket.close();
		}
		catch (IOException ex) {
			System.err.println("I/O exception during execution\n");
			System.exit(1);
		}
	}

	//sends a file from clientFiles to the server
	public void put(String[] command) {
		try {
			socketOutput.write(command[0]);
			socketOutput.newLine();
			socketOutput.flush();

			//send filename to server
			socketOutput.write(command[1]);
			socketOutput.newLine();
			socketOutput.flush();

			filePath = "clientFiles/" + command[1];
			File file = new File(filePath);

			if (file.exists()) {
				//let server know if file exists
				socketOutput.write("File found");
				socketOutput.newLine();
				socketOutput.flush();

				putFile = new BufferedInputStream(new FileInputStream(file));
				//temporary variable to store what is read from the file
				int temp = 0;
				//read from file and write to server
				while ((temp = putFile.read()) != -1) {
					socketOutput.write(temp);
				}
				socketOutput.flush();
				putFile.close();
				System.out.println("File sent");
			} else {
				System.err.println("File not found");
				socketOutput.write("File does not exist");
				socketOutput.newLine();
				socketOutput.flush();
			}
		}
		catch (IOException ex) {
			System.err.println("I/O exception during execution\n");
			System.exit(1);
		}
	}

	public static void main(String[] args) {
		if (args.length == 0) {
			System.err.println("No command provided");
			System.exit(1);
		} else if (args.length > 2) {
			System.err.println("Too many commands provided");
			System.exit(1);
		}

		Client c = new Client();

		//list
		if (args[0].equals("list")) {
			if (args.length > 1) {
				System.err.println("The list command should be used on its own");
				System.exit(1);
			}
			c.list(args[0]);

		//get
		} else if (args[0].equals("get")) {
			if (args.length == 1) {
				System.err.println("No filename provided");
				System.exit(1);
			}
			c.get(args);

		//put
		} else if (args[0].equals("put")) {
			if (args.length == 1) {
				System.err.println("No filename provided");
				System.exit(1);
			}
			c.put(args);
		} else {
			System.out.println(args[0] + " is not a command");
		}
	}
}
