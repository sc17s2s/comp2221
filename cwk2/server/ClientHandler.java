import java.net.*;
import java.io.*;
import java.util.*;
import java.text.*;

//handles each client for the server
public class ClientHandler extends Thread {
  private Socket socket = null;       //client socket
  private BufferedReader in = null;   //to read from client
  private BufferedWriter out = null;  //to write to client
  private String command = null;
  private String fileName = null;
  private String filePath = null;
  private BufferedInputStream sendFile = null;        //send to client
  private BufferedOutputStream receiveFile = null;    //get from client
  private String dateTime = null;
  private BufferedWriter writeLog = null;  //to write to log.txt
  private String log = null;

  public ClientHandler(Socket socket) {
		super("ClientHandler");
		this.socket = socket;
  }

  public void run() {
    try {
      in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      out = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

      //initialise date, time, client IP address to write logs
      writeLog = new BufferedWriter(new FileWriter("log.txt", true));
      InetAddress inet = socket.getInetAddress();
      SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyy:HH:mm:ss:");
      Date date = new Date();
      dateTime = format.format(date);

      //read command sent from client
      command = in.readLine();

      //list
      if (command.equals("list")) {
        File folder = new File("serverFiles");
        //array of files that contains the files in serverFiles
        File[] files = folder.listFiles();

        //send all the filenames to client
        for (File f : files) {
          out.write(f.getName());
          out.newLine();
        }
        out.flush();

        //write log for client request
        log = dateTime + inet.getHostAddress() + ":" + command + "\n";
        writeLog.append(log);
        writeLog.flush();
      }

      //get
      else if (command.equals("get")) {
        //read filename from client
        fileName = in.readLine();
        filePath = "serverFiles/" + fileName;
        File file = new File(filePath);

        if (file.exists()) {
          //let client know if file exists
          out.write("File found");
          out.newLine();
          out.flush();

          sendFile = new BufferedInputStream(new FileInputStream(file));
          //temporary variable to store what is read from the file
          int temp = 0;
          //read from file and write to client
          while ((temp = sendFile.read()) != -1) {
            out.write(temp);
          }
          out.flush();
          sendFile.close();
          System.out.println("File sent");

          //write log for client request
          log = dateTime + inet.getHostAddress() + ":" + command + " " + fileName + "\n";
          writeLog.append(log);
          writeLog.flush();
        } else {
          System.err.println("File not found");
          out.write("File does not exist");
          out.newLine();
          out.flush();
        }
      }

      //put
      else if (command.equals("put")) {
        //read filename from client
        fileName = in.readLine();

        //read from client whether the file exists
        String fileExists = in.readLine();
        System.out.println(fileExists);

        //only executes code if file is found
        if (fileExists.equals("File found")) {
          //file to save in serverFiles
          File path = new File("serverFiles/" + fileName);
          receiveFile = new BufferedOutputStream(new FileOutputStream(path));

          //temporary variable to store what is read from client
    			int temp = 0;
          //read from client and write to file
          while ((temp = in.read()) != -1) {
    				receiveFile.write(temp);
    			}
    			receiveFile.flush();
    			System.out.println("File received");
    			receiveFile.close();

          //write log for client request
          log = dateTime + inet.getHostAddress() + ":" + command + " " + fileName + "\n";
          writeLog.append(log);
          writeLog.flush();
        }
      }
      writeLog.close();
      in.close();
      out.close();
      socket.close();
    } catch (IOException ex) {
        System.err.println("error");
    }
  }
}
