import java.io.*;
import java.net.*;
import java.util.concurrent.*;

//a server with 10 threads
public class Server {
	private ServerSocket server = null;
	private ExecutorService service = null;
	private Socket client = null;
	private ClientHandler handler = null;

	public Server() {
    try {
      server = new ServerSocket(8888);
			System.out.println("Server connected");
    } catch (IOException ex) {
        System.err.println("Could not listen on port 8888");
        System.exit(1);
    }
		//initialise the executor for multithreading
		service = Executors.newFixedThreadPool(10);
	}

	//runs continuously to accept clients and submits a new handler to the thread pool for each client
	public void runServer() {
		try {
			while(true) {
				client = server.accept();
				handler = new ClientHandler(client);
				service.submit(handler);
			}
		} catch (IOException ex) {
				System.err.println("Client accept failed");
				System.exit(1);
		}
	}

	public static void main(String[] args) throws IOException {
		Server s = new Server();
		s.runServer();
  }
}
