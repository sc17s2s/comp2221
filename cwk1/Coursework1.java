import java.net.*;
import java.io.*;

// turns a hostname into an IP address
public class Coursework1 {

  private InetAddress inet = null;
  public static byte[][] addresses = null;
  public static int input;                  // stores the number of hostnames entered
  public static int[] hierarchy;
  public static int count;

  public Coursework1(String host) {
    setInet(host);
  }

  public void setInet(String host) {
    try {
      inet = InetAddress.getByName(host);
    } catch (UnknownHostException ex) {
      ex.printStackTrace();
    }
  }

  public InetAddress getInet() {
    return inet;
  }

  public String getName() {
    return inet.getHostName();
  }

  public String getIPAddress() {
    return inet.getHostAddress();
  }

  public String getCanonicalName() {
    return inet.getCanonicalHostName();
  }

  public byte[] getByteAddress() {
    return inet.getAddress();
  }

  // prints out the details of an IP address
  public void resolve() {
      System.out.println("Hostname: " + getName());
      System.out.println("IP Address: " + getIPAddress());
      System.out.println("Canonical Hostname: " + getCanonicalName());

      if (inet instanceof Inet6Address) {
        System.out.println("This address is IPv6");
      } else if (inet instanceof Inet4Address) {
        System.out.println("This address is IPv4");
      }

      try {
        if (inet.isReachable(100)) {
          System.out.println(getName() + " is reachable");
        } else {
          System.out.println("Not reachable");
        }
      } catch (IOException ex) {
        ex.printStackTrace();
      }
      System.out.println("------------------------------------------------------");
  }

  public static void main(String[] args) {
    input = args.length;

    if (input == 0) {
      System.err.println("No hostname entered");
      System.exit(1);
    }

    byte[][] addresses = new byte[input][];   // stores all IP addresses if IPv4
    int[] hierarchy = new int[4];     // stores the highest level of hierarchy that all IPv4 addresses share
    count = 0;                        // counts the number of IPv4 addresses

    for (int i = 0; i < input; i++) {
      Coursework1 cwk = new Coursework1(args[i]);   // creates a Coursework1 for each hostname entered
      cwk.resolve();
      if (cwk.getByteAddress().length < 5) {        // add IPv4 addresses only to the array
        addresses[count] = cwk.getByteAddress();
        count++;
      }
    }

    // check for the highest levels of hierarchy that all the IPv4 addresses share
    try {
      if (addresses[1] != null) {     // only do if more than one IPv4 address
        // check first byte of each IPv4 address
        for (int i = 0; i < count-1; i++) {
          if (addresses[i][0] != addresses[i+1][0]) {   // if highest level is not equal
            System.out.println("Highest level of IPv4 address hierarchy: *.*.*.*");
            System.exit(0);     // exit program, otherwise continue checking next levels
          }
        }
        hierarchy[0] = addresses[0][0] & 0xFF;    // add first byte to hierarchy array as unsigned

        // check second byte of each IPv4 address
        for (int i = 0; i < count-1; i++) {
          if (addresses[i][1] != addresses[i+1][1]) {
            System.out.println("Highest level of IPv4 address hierarchy: " + hierarchy[0] + ".*.*.*");
            System.exit(0);
          }
        }
        hierarchy[1] = addresses[0][1] & 0xFF;

        // check third byte of each IPv4 address
        for (int i = 0; i < count-1; i++) {
          if (addresses[i][2] != addresses[i+1][2]) {
            System.out.println("Highest level of IPv4 address hierarchy: " + hierarchy[0]
                               + "." + hierarchy[1] + ".*.*");
            System.exit(0);
          }
        }
        hierarchy[2] = addresses[0][2] & 0xFF;

        // check fourth byte of each IPv4 address
        for (int i = 0; i < count-1; i++) {
          if (addresses[i][3] != addresses[i+1][3]) {
            System.out.println("Highest level of IPv4 address hierarchy: " + hierarchy[0]
                               + "." + hierarchy[1] + "." + hierarchy[2] + ".*");
            System.exit(0);
          }
        }
        hierarchy[3] = addresses[0][3] & 0xFF;
        System.out.println("Highest level of IPv4 address hierarchy: " + hierarchy[0]
                           + "." + hierarchy[1] + "." + hierarchy[2] + "." + hierarchy[3]);
      } else {
        System.out.println("No hierarchy comparison done");   // when there is only one IPv4 address
      }
    } catch (ArrayIndexOutOfBoundsException ex) {   // when only one hostname is entered
      System.out.println("No hierarchy comparison done");
    }
  }
}
